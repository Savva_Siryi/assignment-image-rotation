#include "image.h"

struct image *rotate90(struct image const *source){
  struct image *rotated = createImage(source->height, source->width);
  if(rotated != NULL) {
      for (size_t y = 0; y < source->height; y++) {
          for (size_t x = 0; x < source->width; x++) {
              rotated->data[rotated->width * x + source->height - 1 - y] = source->data[source->width * y + x];
          }
      }
      return rotated;
  }
  return NULL;
}
