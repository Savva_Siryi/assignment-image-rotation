#include "bmp.h"
#include "rotate.h"
#include "util.h"
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char **argv) {
    if (argc != 3) {
        printf("Enter the path to the incoming and outgoing files\n");
        return 1;
    }

    FILE *inputFile = NULL, *outputFile = NULL;
    struct image *inputImage = NULL, *outputImage = NULL;

    enum open_status openStatus = openFile(&inputFile, argv[1], "rb");
    if(openStatus != OPEN_OK){
        fprintf(stderr, "No such file or directory\n");
        return 1;
    }
    fprintf(stdin, "Input file opened\n");


    enum read_status readStatus = fromBmp(inputFile, &inputImage);
    switch (readStatus) {
        case READ_OK:
            fprintf(stdin, "Image loaded successfully\n");
            break;
        case READ_INVALID_HEADER:
            fprintf(stderr, "File has an invalid header\n");
            return 1;
        case READ_FAILED:
            fprintf(stderr, "Read error appeared\n");
            return 1;
        case READ_INVALID_BITS:
            fprintf(stderr, "The image not in RGB\n");
            return 1;
    }

    if(closeFile(inputFile) != CLOSE_OK){
        fprintf(stderr, "Input file cant close\n");
        return 1;
    }
    outputImage = rotate90(inputImage);
    if(outputImage == NULL){
        fprintf(stderr, "Rotate image fail\n");
        return 1;
    }
    freeImage(inputImage);

    openStatus = openFile(&outputFile, argv[2], "wb");
    if (openStatus != OPEN_OK) {
        fprintf(stderr, "No such file or directory\n");
        return 1;
    }
    fprintf(stdin, "Input file opened\n");


    enum write_status writeStatus = toBmp(outputFile, outputImage);
    switch (writeStatus) {
        case WRITE_OK:
            fprintf(stdin, "Image saved successfully\n");
            break;
        case WRITE_INVALID_SOURCE:
            fprintf(stderr, "Image broke 2\n");
            return 1;
        case WRITE_HEADER_ERROR:
            fprintf(stderr, "Error appeared while writing new image's header\n");
            return 1;
        case WRITE_ERROR:
            fprintf(stderr, "Error appeared while writing new image\n");
            return 1;
    }
    if(closeFile(outputFile) != CLOSE_OK){
        fprintf(stderr, "Output file cant close\n");
        return 1;
    }
    freeImage(outputImage);
    return 0;
}
