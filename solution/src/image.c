#include "image.h"
#include <inttypes.h>
#include <stdlib.h>



struct image* createImage(uint64_t width, uint64_t height) {
    struct image *img = malloc(sizeof(struct image));
    if(img != NULL) {
        img->width = width;
        img->height = height;
        img->data = malloc(sizeof(struct pixel) * width * height * 3);
        return img;
    }
    return NULL;
}

void freeImage(struct image* img){
    if(img != NULL) {
        free(img->data);
        free(img);
    }
}
