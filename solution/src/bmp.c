#include <inttypes.h>
#include <stdio.h>

#include "bmp.h"
#include "image.h"

static bmpHeader createHeader(struct image const *img) {
    bmpHeader bmpHeader = {0};
    if(img != NULL) {
        size_t padding = (4 - (img->width * sizeof(struct pixel) % 4)) % 4;
        bmpHeader.bfType = 0x4D42;
        bmpHeader.bfReserved = 0;
        bmpHeader.bOffBits = 54;
        bmpHeader.biSize = 40;
        bmpHeader.biWidth = img->width;
        bmpHeader.biHeight = img->height;
        bmpHeader.biPlanes = 1;
        bmpHeader.biBitCount = 24;
        bmpHeader.biCompression = 0;
        bmpHeader.biSizeImage = (img->width * sizeof(struct pixel) + padding) * img->height;
        bmpHeader.bfileSize = bmpHeader.biSizeImage + sizeof(bmpHeader);
        bmpHeader.biXPelsPerMeter = 0;
        bmpHeader.biYPelsPerMeter = 0;
        bmpHeader.biClrUsed = 0;
        bmpHeader.biClrImportant = 0;
    }
    return bmpHeader;
}


uint8_t getOffset(bmpHeader header) {
    uint8_t offset = (3*header.biWidth) % 4;
    if (offset != 0) {
        offset = 4 - offset;
    }
    return offset;
}

enum read_status fromBmp(FILE* in, struct image** img ){
  bmpHeader header;
  struct pixel pixel;
    if(fread(&header, sizeof(bmpHeader), 1, in) != 1){
        return READ_FAILED;
    }
  if(header.biBitCount != 24){
    return READ_INVALID_BITS;
  }

  *img = createImage(header.biWidth, header.biHeight);
  uint8_t offset = getOffset(header);

  for (size_t i = 0; i < header.biHeight; i++){
    for (size_t j = 0; j < header.biWidth; j++) {
        if(fread(&pixel, sizeof(struct pixel), 1, in) != 1){
            return READ_FAILED;
        }
      (*img)->data[header.biWidth * i + j] = pixel;
    }

    if (offset != 0){
        if(fread(&pixel, sizeof(unsigned char)*offset, 1, in) != 1){
            return READ_FAILED;
        }
    }
  }

  return READ_OK;
}

enum write_status toBmp( FILE* out, struct image const* img ){
  struct pixel pixel;
  bmpHeader bmpHeader = createHeader(img);

  uint8_t offset = getOffset(bmpHeader);

  if(fwrite(&bmpHeader, sizeof(bmpHeader), 1, out) != 1){
      return WRITE_ERROR;
  }
  if(fseek(out, bmpHeader.bOffBits, SEEK_SET) != 0){
      return WRITE_HEADER_ERROR;
  }

  for (size_t i = 0; i < img->height; i++) {
    for (size_t j = 0; j < img->width; j++) {
      pixel = img->data[img->width * i + j];
      if(fwrite(&pixel, sizeof(struct pixel), 1, out) != 1){
          return WRITE_ERROR;
      }
    }
    if (offset != 0) {
      for (size_t j = 0; j < offset; j++) {
        pixel.b = 0;
        pixel.r = 0;
        pixel.g = 0;
        if(fwrite(&pixel, sizeof(unsigned char), 1, out) != 1){
            return WRITE_ERROR;
        }
      }
    }
  }
  return WRITE_OK;
}
