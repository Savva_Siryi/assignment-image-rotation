#include "status.h"
#include <stdio.h>

enum open_status openFile(FILE **file, const char *path, const char *modes);

enum close_status closeFile(FILE *file);
