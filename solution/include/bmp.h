#include "image.h"
#include "status.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

typedef struct __attribute__((packed)) {
uint16_t bfType;
uint32_t bfileSize;
uint32_t bfReserved;
uint32_t bOffBits;
uint32_t biSize;
uint32_t biWidth;
uint32_t biHeight;
uint16_t biPlanes;
uint16_t biBitCount;
uint32_t biCompression;
uint32_t biSizeImage;
uint32_t biXPelsPerMeter;
uint32_t biYPelsPerMeter;
uint32_t biClrUsed;
uint32_t biClrImportant;
} bmpHeader;

enum read_status fromBmp(FILE *in, struct image **img);

enum write_status toBmp(FILE *out, struct image const *img);
