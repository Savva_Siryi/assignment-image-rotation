#ifndef IMAGE
#define IMAGE

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>


struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct pixel {
    uint8_t b, g, r;
};


struct image* createImage(uint64_t width, uint64_t height);

void freeImage(struct image* img);

#endif
